Behavior = function(dialog) {
	this._dialog = dialog;
};

Behavior.prototype.attachEvent = function() {};

Behavior.prototype.detachEvent = function() {};

NullBehavior = function(dialog) {
	Behavior.call(this, dialog);
};

NullBehavior.contructor = NullBehavior;
NullBehavior.prototype = new Behavior();

FirstBehavior = function(dialog) {
	Behavior.call(this, dialog);
};

FirstBehavior.contructor = FirstBehavior;
FirstBehavior.prototype = new Behavior();

FirstBehavior.prototype.attachEvent = function() {
	var self = this;
	$(document).on('click', '.yes-btn', function(e) {
		// validation 

		var form = $( "form.form-data" );
		var formdata = [];
		var fields = [];

		form.find(":input").each( function( idx, val ) {
			var _name = $(val).attr("name");

			if ( $(val).is( "button" ) || $(val).is("input[type=button]" ) )
				return true;

			if ( fields.indexOf( _name ) == -1 )
				fields.push( _name );
		} );

		form.find( ".message" ).remove();

		var messages = {
			required : "This field is required",
			email : "Please input valid email address",
			phone : "Please input valid phone number",
			number : "Please input numbers",
			min : "Minimum charactors for this field is invalid",
			max : "Maximum charactors for this field is invalid",
			atleast : "Select at least minimum required checkboxes",
			atmost : "Select at most maximum required checkboxes"
		};

		fields.forEach( function(field) {
			var ipt = form.find( "[name=" + field + "]" );
			var val = ipt.val();

			var flag = "";
			var pattern;

			if ( ipt.is("[type=checkbox]") || ipt.is("[type=radio]") ) {
				var tpt = ipt.parent();
				var cvals = [];

				form.find( "[name=" + field + "]:checked" ).each( function( idx, cobj ) {
					cvals.push( $(cobj).val() );
				} );

				if ( tpt.data( 'required' ) ) {
					if ( cvals.length === 0 ) {
						flag = "required";
					}
				}
				if ( flag.length === 0 && tpt.data( 'at-least' ) ) {
					if ( cvals.length < tpt.data( 'at-least' ) ) {
						flag = "atleast";
					}
				}
				if ( flag.length === 0 && tpt.data( 'at-most' ) ) {
					if ( cvals.length > tpt.data( 'at-most' ) ) {
						flag = "atmost";
					}
				}
				if ( flag.length > 0 ) { 
					if ( tpt.find( ".message").length === 0 ) {
						tpt.append( '<div class="message">' + messages[flag] + '</div>' );
					}
				}

				val = cvals.join( ',' );
			}
			else {

				if ( ipt.data('required') ) {
					if ( val.length === 0 ) {
						flag = "required";
					}
				}

				if ( flag.length === 0 && ipt.data( 'valid-email' ) ) {
					pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
					if ( ! pattern.test( val ) ) {
						flag = "email";
					}
				}
				
				if ( flag.length === 0 && ipt.data( 'valid-phone' ) ) {
					pattern = /^(8|9)\d{7}$/;
					if ( ! pattern.test( val ) ) {
						flag = "phone";
					}
				}
				
				if ( flag.length === 0 && ipt.data( 'valid-number' ) ) {
					pattern = /^(\d+)$/;
					if ( ! pattern.test( val ) ) {
						flag = "number";
					}
				}

				if ( ipt.data( 'valid-min' ) || ipt.data( 'valid-max' ) ) {
					if ( flag.length === 0 ) {
						if ( val.length < ipt.data( 'valid-min' ) ) {
							flag = "min";
						}
						else if ( val.length > ipt.data( 'valid-max' ) ) {
							flag = "max";
						}
					}
				}
	 
				if ( flag.length > 0 ) { 
					ipt.after( '<div class="message">' + messages[flag] + '</div>' );
				}
			}

			formdata.push( { name:field, value:val } );
		} );

		if ( form.find( ".message" ).length > 0 ) {
			e.preventDefault();
			e.stopPropagation();
			return;
		}
		else {
			$.ajax({
				url: '/saveform',
				type: 'POST',
				data: formdata
			}).done(function (data) {
				// self.filename = data.filename;
				// $('.yes-btn').prop('disabled', false).text("Yes");
			});
		}

		// end of validation

		self.detachEvent();
		self._dialog.next(new ChoosePhotoStyle(self._dialog));
	});

	$(document).on('click', '.btn-next', function(e) {
		self.detachEvent();
		self._dialog.next(new ChoosePhotoStyle(self._dialog));
	});
};

FirstBehavior.prototype.detachEvent = function() {
	$(document).off('click', '.yes-btn');
	$(document).off('click', '.btn-next');
};

ChoosePhotoStyle = function(dialog) {
	Behavior.call(this, dialog);
};

ChoosePhotoStyle.contructor = ChoosePhotoStyle;
ChoosePhotoStyle.prototype = new Behavior();


ChoosePhotoStyle.prototype.attachEvent = function() {
	var self = this;

	$(document).on('click', '.print-btn', function() {
		//self._dialog.next(new Printout(self._dialog));
		//self.detachEvent();
		self._dialog.nextWithoutBehavior();
	});

	$(document).on('click', '.mms-btn', function() {
		self.detachEvent();
		self._dialog.nextMMSBehavior(new Printout(self._dialog));
	});

	$(document).on('click', '.email-btn', function() {
		var img_style = [];
		var print_amount = $("select.print-amount").val() || 1;

		$(":checkbox.image-type").each(function() {
			if (this.checked) {
				img_style.push($(this).val());
			}
		});

		if (img_style.length) {

			self.detachEvent();

			self._dialog.setImgType(img_style.join(","));
			self._dialog.setPrintAmount(print_amount);
			//   self._dialog.uploadFileForEmail();
			
			if ( $( "form.form-data" ).length > 0 ) {
				self._dialog.NextBehaviorStep(new Printout(self._dialog), 3 );
			}
			else {
				self._dialog.NextBehaviorStep(new Printout(self._dialog), 2 );
			}
			//self._dialog.next(new Printout(self._dialog));
		}
	});

	$(document).on('click', '.next-image-style', function() {
		var img_style = [];
		var print_amount = $("select.print-amount").val() || 1;

		$(":checkbox.image-type").each(function() {
			if (this.checked) img_style.push($(this).val());
		});

		if (img_style.length) {
			self.detachEvent();

			self._dialog.setImgType(img_style.join(","));
			self._dialog.setPrintAmount(print_amount);
			self._dialog.print();
			$("#myModal").modal("hide");
			//self._dialog.next(new Printout(self._dialog));

		}
		else $(".err-msg").removeClass("hide");
	});
};
ChoosePhotoStyle.prototype.detachEvent = function() {
		$(document).off('click', '.next-image-style');
		$(document).off('click', '.email-btn');
		$(document).off('click', '.print-btn');
		$(document).off('click', '.mms-btn');
		$(".err-msg").addClass("hide");
	};
	/**
	 * print out action
	 */
Printout = function(dialog) {
	Behavior.call(this, dialog);
};

Printout.contructor = Printout;
Printout.prototype = new Behavior();

Printout.prototype.attachEvent = function() {
	var self = this;
	$(".submit-email").click(function(e) {
		e.preventDefault();

		var email = $("#emailInput").val();
		var name = $("#nameInput").val();
		var address = $("#addressInput").val();
		if (!self._isValidEmail(email)) { //got error, show error message,
			$("#emailInput").focus().parent().addClass("has-error");
			$(".err-msg").removeClass("hide");
			//$(".modal-btn-grp-3 button").removeClass("disabled");

		} else {
			self.detachEvent();

			//self._dialog.sendMail(email);
			self._dialog.sendLogMail(email, name, address);

			self._dialog.next(new NullBehavior(self._dialog));
			$("#myModal").modal("hide");
		}
		return false;
	});

	$(".submit-mms").click(function(e) {
		var phone = $("#phoneInput").val();
		if (!self._isValidPhone(phone)) { //got error, show error message,
			$("#phoneInput").focus().parent().addClass("has-error");
			$(".err-msg").removeClass("hide");
		} else {
			self.detachEvent();
			self._dialog.next(new NullBehavior(self._dialog));
			self._dialog.sendMMS(phone);
			$("#myModal").modal("hide");
		}
	});

	this._t = setInterval(function() {
		var email = $("#emailInput").val();
		var mms = $("#phoneInput").val();

		if (email) {
			if (email.length) { //something is in email field. submit button is active
				//$(".modal-btn-grp-3 button").addClass("disabled");
				$(".submit-email").removeClass("disabled");
			} else {
				//$(".modal-btn-grp-3 button").removeClass("disabled");
				$(".submit-email").addClass("disabled");
			}
		}

		if (mms) {
			if (mms.length) {
				//$(".modal-btn-grp-3 button").addClass("disabled");
				$(".submit-mms").removeClass("disabled");
			} else {
				//$(".modal-btn-grp-3 button").removeClass("disabled");
				$(".submit-mms").addClass("disabled");
			}
		}
	}, 500);
};

Printout.prototype.detachEvent = function() {
	$(".submit-email").off('click');
	$(".submit-mms").off('click');
	clearInterval(this._t);
};

Printout.prototype._isValidEmail = function(mail) {
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return filter.test(mail);
};

Printout.prototype._isValidPhone = function(phone) {
	var filter = /^[9|8][0-9]{7}$/;
	return filter.test(phone);
};