var http = require('http');
var https = require('https');
var request = require('request');
var fs = require('fs');
var path = require('path'),
appDir = path.dirname(require.main.filename);
var config = require("../model/config").config;
var nodemailer = require("nodemailer");
var imgFolder = appDir +config.imgfoldername;

/**
 * generate file with random name
 * @param url image url to get extension of file
 * @returns {String} return filename with extension
 */
function getRandomFileName(img, idx){
	var extension = "";
	if ( idx )
		extension = img.url[idx].split('.').pop();
	else 
		extension = img.url.split('.').pop();

	var text = "";
	var possible = "abcdefghijklmnopqrstuvwxyz";
	var copyString = img.amount + 'copy';
	var printString = (Math.round((img.amount)/2));
	//var printString = (Math.ceil((img.amount)/4));


	for( var i=0; i < 4; i++ ) {
	    text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	//for wallet
	return  [[img.username, img.created_time, text,copyString,printString].join('-'), ".", extension].join('');
	//return  [[img.username, img.created_time, text, copyString,img.amount].join('-'), ".", extension].join('');
};

/*
* upload file to dropbox email  folder Email
*/
exports.sendMail = function (req, res) {
	var img = req.body;
	// var filename = getRandomFileName(img);
	var email = img.mail;
	var smtpTransport = nodemailer.createTransport("SMTP", config.smtpTransport);
	var mailOptions = config.mailOptions;
	mailOptions.to=email;
	mailOptions.html='<p>Hi there,</p><p>Here&#39;s the soft copy of the photo you wanted.</p><p>In case you are wondering what we do, we are a one stop event photography agency that helps event organizers market their brand and delight their guests through customised event photography solutions. We are based in Singapore and we specialise in Live Instagram Printing, Photo Booth, Roving Photography, Smartphone Instant Print and Live Photo Feed.</p><p>If you are having an event soon and would like to engage in our service, do let me know.</p><p>Thanks and have a nice day ahead!</p><p>Cheers,<br>Ewan Sou<br>Founder<br>&#43;65 9780 1671<br>ewan&#64;instantly.sg</p><p>Website: <a href="www.instantly.sg" target="_blank">www.instantly.sg</a><br>Facebook: <a href="www.facebook.com/instantly.sg" target="_blank">www.facebook.com/instantly.sg</a><br>Instagram: <a href="www.instagram.com/instantly.sg" target="_blank">www.instagram.com/instantly.sg</a></p>';

	if ( Array.isArray( img.url ) )
	{
		mailOptions.attachments = [];

		img.url.forEach( function( val, idx ) {
			var locationPath=appDir+"/public"+val;

			mailOptions.attachments.push( { // utf-8 string as an attachment
				filename: 'yourphoto-' + (idx+1).toString() + '.jpg',
				streamSource: fs.createReadStream(locationPath),
				contentType: 'image/jpeg'
			} );
		} );
	}
	else {
		var locationPath=appDir+"/public"+img.url;

		mailOptions.attachments =[{ // utf-8 string as an attachment
			filename: 'yourphoto.jpg',
			streamSource: fs.createReadStream(locationPath),
			contentType: 'image/jpeg'
		}]
	}
	// console.log( mailOptions );

	smtpTransport.sendMail(mailOptions, function() {
		smtpTransport.close();
		res.send({ // file on disk as an attachment
			filename: ''
		});
	});
};
