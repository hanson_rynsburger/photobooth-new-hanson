var express = require("express");
var app = express();
var port = process.env.PORT || 3800;
var http = require('http');
var request = require('request');
var fs = require('fs');
var path = require('path');
var config = require("./model/config").config;
var logger = require('./model/log');
var uploader = require('./routes/upload');
var mms = require("./routes/mms");
var nodemailer = require("nodemailer");
var chokidar = require('chokidar');

var lastCursor = null;

var numberPrints;
const numberPrintsFile = __dirname + "/numberprints.json";

try {
  numberPrints = JSON.parse( fs.readFileSync(numberPrintsFile, "utf8" ) );
}
catch (err) {
	numberPrints = {};
  	fs.writeFileSync(numberPrintsFile, JSON.stringify( {} ), "utf8");
}

/**
 * Set the paths for your files
 * @type {[string]}
 */
var pub = __dirname + '/public',
	view = __dirname + '/views',
	photos = '/photos/',
	photosPub = '/public' + photos,
	watchPath = './watch/',
	originalPics = './originalPics/',
	toPrint = './toPrint/';

/**
 * Set your app main configuration
 */
app.configure(function(){
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express.static(pub));
	app.use(express.static(view));
	app.use(express.errorHandler());
});

/**
 * Render your index/view "my choice was not use jade"
 */
app.get("/views", function(req, res){
	res.render("index");
});

/**
 * send email
 */
app.post('/sendMail', uploader.sendMail);

/**
 * send mms
 */
app.post('/sendmms',mms.sendmms);

app.get('/reset', function(req, res) {
	fs.writeFileSync(numberPrintsFile, JSON.stringify( {} ), "utf8");
	numberPrints = {};
	res.send("Print data cleared");
});

function startWatchers(socket) {
	var gifwatcher = chokidar.watch(watchPath + '*.gif', {
		ignored: /[\/\\]\./,
		persistent: true
	});

	var GIFwatcher = chokidar.watch(watchPath + '*.GIF', {
		ignored: /[\/\\]\./,
		persistent: true
	});

	// image added
	var addCB = function(filePath) {
		moveFile(filePath, function(err, filename) {
			if(!err) {
				var filestats = fs.statSync( filePath );

				filename = filename.replace('public/', '');
				filename = filename.replace('public\\', '');

				var np = (numberPrints[filename])?numberPrints[filename]:0;
				socket.emit('addImage', { url: filename, ctime: filestats.mtime.getTime(), np: np });
			}
		});

		var fileName = filePath.replace('watch/', '');
		fileName = fileName.replace('watch\\', '');
		console.log('File', fileName, 'has been added');
	};
	// image changed
	var changeCB = function(filePath) {
		moveFile(filePath, function(err, filename) {
			if(!err) {
				console.log('socket sent');
				filename = filename.replace('public/', '');
				filename = filename.replace('public\\', '');
				socket.emit('updateImage', { url: filename });
			}
		});

		filePath = filePath.replace('watch/', '');
		filePath = filePath.replace('watch\\', '');
		console.log('File', filePath, 'has been changed');
	};
	// image removed
	var unlinkCB = function(filePath) {
		deleteFile(filePath, function(err, filename) {
			filename = filename.replace('public/', '');
			filename = filename.replace('public\\', '');
			socket.emit('removeImage', { url: filename });
		});

		filePath = filePath.replace('watch/', '');
		filePath = filePath.replace('watch\\', '');
		console.log('File', filePath, 'has been removed');
	};

	gifwatcher.on('add', addCB);
	gifwatcher.on('change', changeCB);
	gifwatcher.on('unlink', unlinkCB);

	GIFwatcher.on('add', addCB);
	GIFwatcher.on('change', changeCB);
	GIFwatcher.on('unlink', unlinkCB);
}

function moveFile(filePath, cb) {
	var name = path.basename(filePath);
	var filename = photosPub + name;

	var source = fs.createReadStream(filePath);
	var dest = fs.createWriteStream(__dirname + filename);

	source.pipe(dest);
	source.on('end', function() { /* copied */
		cb(null, filename);
	});
	source.on('error', function(err) { /* error */
		cb(err, filename);
	})
}

function deleteFile(filePath, cb) {
	var name = path.basename(filePath);
	var filename = photosPub + name;
	var filenameDir = path.join(__dirname, filename);

	fs.unlink(filenameDir, function (err) {
		cb(err, filename);
	});
}

var io = require('socket.io').listen(app.listen(port));

io.configure(function () {
	io.set("transports", ["xhr-polling"]);
	io.set("polling duration", 10);
});

function initializeFilter( socket ) {
	var timefilter = [];

	for ( var p = config.filter_time_from; p<config.filter_time_to; p++) {
		for ( var mm = 0; mm<60; mm+=config.filter_interval) {
			timefilter.push( leadingZero( p ) + ":" + leadingZero( mm ) );
		}
	}
	timefilter.push( config.filter_time_to + ":00");

	socket.emit( 'initFilter', timefilter );
}

function leadingZero( val ) {
	if ( parseInt( val ) < 10 ) 
		return "0" + parseInt(val);
	else 
		return val;
}

io.sockets.on('connection', function (socket) {
	// starting watcher
	startWatchers(socket);

	initializeFilter( socket );

	// events
	socket.on('print', function(data) {
		var url = data.url;
		var amount = data.amount;
		var copyString = amount + 'copy';
		var printString = (Math.round((amount)/2));
		//var printString = amount;
		var text = "";
		var possible = "abcdefghijklmnopqrstuvwxyz";
		var appDir = path.dirname(require.main.filename);
		var imgFolder = appDir + config.imgfoldername;
		var basename = path.basename(url, path.extname(url));
		var filename = basename + path.extname(url);

		for( var i=0; i < 11; i++ ) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}

		//var printTempFilename =	[basename, "-", text, "-", copyString,"-",printString, path.extname(url)].join('');
		var printTempFilename =	[basename, "-", text, "-", copyString,"-",printString, ".jpg"].join('');

		/*
		fs.exists('./originalPics/' + filename, function (exists) {
			if(exists) fs.createReadStream(originalPics + filename).pipe(fs.createWriteStream(toPrint + printTempFilename));
		});*/

		fs.readdir(originalPics, function (err, files) {
			for(var i=0; i< files.length; i++) {
				if(files[i].indexOf(basename) != -1) {
					fs.createReadStream(originalPics + files[i]).pipe(fs.createWriteStream(toPrint + printTempFilename));
				}
			}
		});

		if ( numberPrints[url] )
			numberPrints[url] = parseInt( numberPrints[url] ) + parseInt( amount );
		else 
			numberPrints[url] = amount;

		fs.writeFile( numberPrintsFile, 
			JSON.stringify( numberPrints ), 
			function (err) {
				if ( err ) { 
					console.log( err );
				}
			}

		);
	});

});
