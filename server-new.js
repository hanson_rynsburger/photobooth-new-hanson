var express = require("express");
var app = express();
var port = process.env.PORT || 3800;
var http = require('http');
var request = require('request');
var fs = require('fs');
var path = require('path');
var config = require("./model/config").config;
var logger = require('./model/log');
var uploader = require('./routes/upload');
var mms = require("./routes/mms");
var nodemailer = require("nodemailer");
var chokidar = require('chokidar');

var lastCursor = null;

/**
 * Set the paths for your files
 * @type {[string]}
 */
var pub = __dirname + '/public',
	view = __dirname + '/views',
	photos = '/photos/',
	photosPub = '/public' + photos,
	watchPath = './watch/',
	originalPics = './2015-10-18/prints/',
	toPrint = './toPrint/';

/**
 * Set your app main configuration
 */
app.configure(function(){
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express.static(pub));
	app.use(express.static(view));
	app.use(express.errorHandler());
});

/**
 * Render your index/view "my choice was not use jade"
 */
app.get("/views", function(req, res){
	res.render("index");
});

/**
 * send email
 */
app.post('/sendMail', uploader.sendMail);

/**
 * send mms
 */
app.post('/sendmms',mms.sendmms);

function startWatchers(socket) {
	var jpgwatcher = chokidar.watch(watchPath + '*.jpg', {
		ignored: /[\/\\]\./,
		persistent: true
	});

	var JPGwatcher = chokidar.watch(watchPath + '*.JPG', {
		ignored: /[\/\\]\./,
		persistent: true
	});

	// image added
	var addCB = function(filePath) {
	moveFile(filePath, function(err, filename) {
		if(!err) {
			filename = filename.replace('public/', '');
			filename = filename.replace('public\\', '');
			socket.emit('addImage', { url: filename });
		}
	});

	var fileName = filePath.replace('watch/', '');
	fileName = fileName.replace('watch\\', '');
	console.log('File', fileName, 'has been added');
	};
	// image changed
	var changeCB = function(filePath) {
	moveFile(filePath, function(err, filename) {
		if(!err) {
			console.log('socket sent');
			filename = filename.replace('public/', '');
			filename = filename.replace('public\\', '');
			socket.emit('updateImage', { url: filename });
		}
	});

	filePath = filePath.replace('watch/', '');
	filePath = filePath.replace('watch\\', '');
	console.log('File', filePath, 'has been changed');
	};
	// image removed
	var unlinkCB = function(filePath) {
	deleteFile(filePath, function(err, filename) {
		filename = filename.replace('public/', '');
		filename = filename.replace('public\\', '');
		socket.emit('removeImage', { url: filename });
	});

	filePath = filePath.replace('watch/', '');
	filePath = filePath.replace('watch\\', '');
	console.log('File', filePath, 'has been removed');
	};


	jpgwatcher.on('add', addCB);
	jpgwatcher.on('change', changeCB);
	jpgwatcher.on('unlink', unlinkCB);

	JPGwatcher.on('add', addCB);
	JPGwatcher.on('change', changeCB);
	JPGwatcher.on('unlink', unlinkCB);
}

function moveFile(filePath, cb) {
	var name = path.basename(filePath);
	var filename = photosPub + name;

	var source = fs.createReadStream(filePath);
	var dest = fs.createWriteStream(__dirname + filename);

	source.pipe(dest);
	source.on('end', function() { /* copied */
		cb(null, filename);
	});
	source.on('error', function(err) { /* error */
		cb(err, filename);
	})
}

function deleteFile(filePath, cb) {
	var name = path.basename(filePath);
	var filename = photosPub + name;
	var filenameDir = path.join(__dirname, filename);

	fs.unlink(filenameDir, function (err) {
		cb(err, filename);
	});
}

var io = require('socket.io').listen(app.listen(port));

io.configure(function () {
	io.set("transports", ["xhr-polling"]);
	io.set("polling duration", 10);
});

io.sockets.on('connection', function (socket) {
	// starting watcher
	startWatchers(socket);
	// events
	socket.on('print', function(data) {
		var url = data.url;
		var amount = data.amount;
		var copyString = amount + 'copy';
		//var printString = (Math.round((amount)/2)); //for half 4r
		var printString = amount; //for 4r
		var text = "";
		var possible = "abcdefghijklmnopqrstuvwxyz";
		var appDir = path.dirname(require.main.filename);
		var imgFolder = appDir +config.imgfoldername;
		var basename = path.basename(url, path.extname(url));
		var filename = basename + path.extname(url);

		for( var i=0; i < 11; i++ ) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}

		var printTempFilename =	[basename, "-", text, "-", copyString,"-",printString, path.extname(url)].join(''); 

		fs.exists(originalPics + filename, function (exists) {
			if(exists) fs.createReadStream(originalPics + filename).pipe(fs.createWriteStream(toPrint + printTempFilename));
		});
	});

});
